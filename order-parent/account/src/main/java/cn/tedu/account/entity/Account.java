package cn.tedu.account.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/22 9:43
 */
//不添加 mybatis-plus 的注解，如 table、id、field...
//在xml中自己写SQL语句
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    private Long id;
    private Long userId;
    private BigDecimal total;       //总金额
    private BigDecimal used;        //已使用金额
    private BigDecimal residue;     //可用金额
    private BigDecimal frozen;      //冻结金额
}
