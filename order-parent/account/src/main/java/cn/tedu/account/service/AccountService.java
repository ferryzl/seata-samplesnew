package cn.tedu.account.service;

import java.math.BigDecimal;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/22 10:21
 */
public interface AccountService {
    void decrease(Long userId, BigDecimal money);
}
