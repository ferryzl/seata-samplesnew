package cn.tedu.account.mapper;

import cn.tedu.account.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.math.BigDecimal;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/22 9:50
 */
public interface AccountMapper extends BaseMapper<Account> {
    void decrease(Long userId, BigDecimal money);
    Account selectByUserId(Long userId);
}
