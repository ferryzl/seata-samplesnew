package cn.tedu.storage.service;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/22 11:04
 */
public interface StorageService {
    void decrease(Long productId,Integer count);
}
