package cn.tedu.storage.mapper;

import cn.tedu.storage.entity.Storage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/22 10:55
 */
public interface StorageMapper extends BaseMapper<Storage> {
    void decrease(Long productId,Integer count);
}
