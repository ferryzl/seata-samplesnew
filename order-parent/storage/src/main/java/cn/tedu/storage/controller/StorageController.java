package cn.tedu.storage.controller;

import cn.tedu.storage.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/22 11:10
 */
@RestController
public class StorageController {
    @Autowired
    private StorageService storageService;
    @GetMapping("/decrease")
    public String decrease(Long productId,Integer count){
        storageService.decrease(productId, count);
        return "减少库存成功";
    }
}
