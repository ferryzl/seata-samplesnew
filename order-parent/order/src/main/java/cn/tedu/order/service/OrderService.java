package cn.tedu.order.service;

import cn.tedu.order.entity.Order;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/22 11:46
 */
public interface OrderService {
    void create(Order order);
}
