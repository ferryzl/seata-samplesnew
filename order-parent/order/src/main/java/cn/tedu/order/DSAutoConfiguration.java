package cn.tedu.order;

import com.alibaba.druid.pool.DruidDataSource;
import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @Author 作者：小龙猿
 * @Project 项目：seata-at
 * @Time 时间：2021/9/23 9:43
 */
/*
自定义的数据源自动配置类，与spring中默认的数据源自动类配置类冲突，
需要在启动类中排除spring默认的自动配置
 */
@Configuration
public class DSAutoConfiguration {
    // 创建原始数据源对象
    // hikari使用的数据库地址参数不是URL，而是jdbcUrl,所以需在yml文件中添加此配置
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource dataSource(){
        return new HikariDataSource();
        //return new DruidDataSource();
    }
    // 创建数据源代理对象
    @Primary // 首选对象，即当使用autowired注解注入DataSource对象时，如果不能确定注入哪一个，则首选注入这个
    @Bean
    public DataSource dataSourceProxy(DataSource ds){
        return new DataSourceProxy(ds);
    }
}
